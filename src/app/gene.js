// pages/index.js
"use client";
import {
  Table,
  TableBody,
  TableCell,
  TableHeader,
  TableRow,
} from "@/components/ui/table";
import { useEffect, useState } from "react";

function getRandomInt(min, max) {
//   return Math.floor(Math.random() * (max - min)) + min;
return Math.random() < 0.5 ? 0 : 1;
}

const rows = 25;
const columns = 15;
let matrix = []

const MatrixDisplay = () => {
    const [newMatrix, setNewMatrix] = useState([]);

  useEffect(() => {
    // Generate random matrix
    for (let i = 0; i < rows; i++) {
        
      matrix[i] = [];
      for (let j = 0; j < columns; j++) {
        matrix[i][j] = getRandomInt(0, 2);
      }
    }

    const plusCenters = [];

    // Search for the plus patterns and store their centers
    for (let i = 1; i < rows - 1; i++) {
      for (let j = 1; j < columns - 1; j++) {
        if (
          matrix[i][j] === 1 &&
          matrix[i - 1][j] === 1 &&
          matrix[i + 1][j] === 1 &&
          matrix[i][j - 1] === 1 &&
          matrix[i][j + 1] === 1
        ) {
          matrix[i][j] = "*";
          matrix[i - 1][j] = "*";
          matrix[i + 1][j] = "*";
          matrix[i][j - 1] = "*";
          matrix[i][j + 1] = "*";
          const centerRow = i;
          const centerColumn = j;
          plusCenters.push({ row: centerRow, column: centerColumn });
        }
      }
    }

    // Output all found plus pattern centers to the console
    console.log("Coordinates of the centers of plus patterns:");
    console.log(matrix)
    setNewMatrix(matrix)
    plusCenters.forEach((center) => {
      console.log("(" + center.row + ", " + center.column + ")");
    });
  }, []); // Empty dependency array ensures this effect runs only once

  return (
    <div className="bg-gray-100 py-12 px-4">
      <div className="max-w-4xl mx-auto">
        <Table
          id="matrixTable"
          className="table-auto mx-auto border-collapse border border-gray-600"
        >
          <TableHeader>
            <TableRow>
              <TableCell >#</TableCell> {/* Empty cell at the top-left corner */}
              {/* Add numbering to the top */}
              {[...Array(columns)].map((_, index) => (
                <TableCell key={index} className="column-number">
                  {index}
                </TableCell>
              ))}
            </TableRow>
          </TableHeader>
          {/* Generate matrix */}
          <TableBody>
            {matrix.map((row, rowIndex) => {
              console.log(row);
              return (
                <TableRow key={rowIndex}>
                  {/* Add numbering to the left */}
                  <TableCell className="row-number">{rowIndex}</TableCell>
                  {row.map((cell, cellIndex) => (
                    <TableCell key={cellIndex}  className={cell === '*' ? "bg-lime-500" : ""}>{cell}</TableCell>
                  ))}
                </TableRow>
              );
            })}
          </TableBody>
        </Table>
      </div>
    </div>
  );
};

export default MatrixDisplay;
