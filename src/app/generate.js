// pages/index.js
"use client";
import {
  Table,
  TableBody,
  TableCell,
  TableHeader,
  TableRow,
} from "@/components/ui/table";
import { useEffect, useState } from "react";

function getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

const rows = 25;
const columns = 15;

const MatrixDisplay = () => {
  const [matrix, setMatrix] = useState([]);
  const [plusCenters, setPlusCenters] = useState([]);

  useEffect(() => {
    generateMatrix();
  }, []); // Empty dependency array ensures this effect runs only once

  const generatePlus = () => {
    // Search for the plus patterns and store their centers
    let newMatrix = JSON.parse(JSON.stringify(matrix)); // Deep copy of the matrix array
    let newPlusCenters = [...plusCenters];
    setPlusCenters([]) // Shallow copy of the plusCenters array

    for (let i = 1; i < rows - 1; i++) {
      for (let j = 1; j < columns - 1; j++) {
        if (
          newMatrix[i][j] === 1 &&
          newMatrix[i - 1][j] === 1 &&
          newMatrix[i + 1][j] === 1 &&
          newMatrix[i][j - 1] === 1 &&
          newMatrix[i][j + 1] === 1
        ) {
          newMatrix[i][j] = "*";
          newMatrix[i - 1][j] = "*";
          newMatrix[i + 1][j] = "*";
          newMatrix[i][j - 1] = "*";
          newMatrix[i][j + 1] = "*";
          const centerRow = i;
          const centerColumn = j;
          newPlusCenters.push({ row: centerRow, column: centerColumn });
        }
      }
    }

    setPlusCenters(newPlusCenters);
    setMatrix(newMatrix);
  };

  const generateMatrix = () => {
    const newMatrix = [];

    // Generate random matrix
    for (let i = 0; i < rows; i++) {
      newMatrix[i] = [];
      for (let j = 0; j < columns; j++) {
        newMatrix[i][j] = getRandomInt(0, 1); // Generate either 0 or 1
      }
    }

    setMatrix(newMatrix);
    setPlusCenters([])
  };

  return (
    <div className="bg-gray-100 py-12 px-4">
      <div className="max-w-4xl mx-auto">
        <button
          className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded mb-4"
          onClick={generateMatrix}
        >
          Generate Matrix
        </button>

        <button
          className="bg-lime-500 hover:bg-lime-700 text-white font-bold py-2 px-4 rounded mb-4"
          onClick={generatePlus}
        >
          Generate Plus
        </button>
        <Table
          id="matrixTable"
          className="table-auto mx-auto border-collapse border border-gray-600"
        >
          <TableHeader>
            <TableRow>
              <TableCell>#</TableCell>{" "}
              {/* Empty cell at the top-left corner */}
              {/* Add numbering to the top */}
              {[...Array(columns)].map((_, index) => (
                <TableCell key={index} className="column-number">
                  {index}
                </TableCell>
              ))}
            </TableRow>
          </TableHeader>
          {/* Generate matrix */}
          <TableBody>
            {matrix.map((row, rowIndex) => (
              <TableRow key={rowIndex}>
                {/* Add numbering to the left */}
                <TableCell className="row-number">{rowIndex}</TableCell>
                {row.map((cell, cellIndex) => (
                  <TableCell
                    key={cellIndex}
                    className={cell === "*" ? "bg-lime-500" : ""}
                  >
                    {cell}
                  </TableCell>
                ))}
              </TableRow>
            ))}
          </TableBody>
        </Table>
        <div className="mt-4">
          <h2 className="text-lg font-semibold">
            Coordinates of Plus Pattern Centers:
          </h2>
          <ul>
            {plusCenters.map((center, index) => (
              <li key={index}>
                ({center.row}, {center.column})
              </li>
            ))}
          </ul>
        </div>
      </div>
    </div>
  );
};

export default MatrixDisplay;
